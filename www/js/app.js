// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var exampleApp = angular.module('starter', ['ionic', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

exampleApp.controller("ExampleController", function($scope, $cordovaBarcodeScanner) {
 
    function Product (barcode, nome) {
       this.barcode = barcode;
       this.nome = nome;
    }

    $scope.list = new Array();

    $scope.scanBarcode = function() {
        $cordovaBarcodeScanner.scan().then(function(imageData) {
            alert(imageData.text);
            var i = 0;
            var check = false;
            var code = imageData.text;
            while(i < $scope.list.length){
          
            if (code == $scope.list[i].barcode) {
              check = true;
            }
            i++;
            }       
            if (check) {
              if (confirm("Esse codigo de barras ja esta cadastrado. OK = editar produto")) {
                i = 0;
                check = false;
                var name = prompt("Digite aqui o novo nome do produto:");
                while(i < $scope.list.length){
          
                if (name == $scope.list[i].nome) {
                  check = true;
                }
                i++;         
                if (check) {
                  i = 0;
                  check = false;
                  name = prompt("Esse nome ja existe. Escolha um novo nome:");
                }
                }
                i = 0;
                while(i < $scope.list.length){
          
                     if (code == $scope.list[i].barcode) {
                     $scope.list[i].nome = name;
                     break;
                }
                i++;
                }
              }
            }
            else {
            i = 0;
            check = false;
            var name = prompt("Digite aqui o nome do produto:");
            while(i < $scope.list.length){
          
            if (name == $scope.list[i].nome) {
              check = true;
            }
            i++;         
            if (check) {
              i = 0;
              check = false;
              name = prompt("Esse nome ja existe. Escolha um novo nome:");
            }
            }
            var new_product = new Product(code, name);
            $scope.list.push(new_product);
            console.log("Barcode Format -> " + imageData.format);
            console.log("Cancelled -> " + imageData.cancelled);
            }
        }, function(error) {
            console.log("An error happened -> " + error);
        });
    };
     
    $scope.removeProduct = function() {
       var i = 0;
       var name_exists = false;
       var nome = prompt("Digite aqui o nome do produto a ser deletado:");
       while(i < $scope.list.length){
          
          if (nome == $scope.list[i].nome) {
             name_exists = true;
             $scope.list.splice(i,1);
             break;
          }
          i++;
       }
       if (name_exists == false) {
          confirm("Produto nao encontrado.");
       }
    };

    $scope.editProduct = function() {
       var i = 0;
       var check = false;
       var name_exists = false;
       var nome = prompt("Digite aqui o nome do produto a ser editado:");
       while (i < $scope.list.length){
          if (nome == $scope.list[i].nome) {
             name_exists = true;
             break;
          }
          i++;
       }
       i = 0;
       if (name_exists){
       var novo_nome = prompt("Digite aqui o novo nome do produto:");
       while(i < $scope.list.length){
          
            if (novo_nome == $scope.list[i].nome) {
              check = true;
            }
            i++;         
            if (check) {
              i = 0;
              check = false;
              novo_nome = prompt("Esse nome ja existe. Escolha um novo nome:");
            }
       }
       i = 0;
       while(i < $scope.list.length){
          
          if (nome == $scope.list[i].nome) {
             $scope.list[i].nome = novo_nome;
             break;
          }
          i++;
       }
       }
       else {
          confirm("Produto nao encontrado.");
       }
    };

});













